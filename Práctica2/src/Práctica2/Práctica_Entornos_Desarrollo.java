/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Práctica2;

import java.util.Scanner;

/**
 *
 * @author DAM124
 */
public class Práctica_Entornos_Desarrollo {
    public static void main(String[] args) {
        Scanner tc=new Scanner(System.in);
        //Aquí irían las intrucciones de uso del programa para los alumnos de 2º de la ESO (aunque juraría que Física y Química puede ser elegida a partir de 3º de la ESO
        double hora, kilometro, velocidad;
        double pie, pulgada, milla;
        int opc;
        do{
        opc=tc.nextInt();
        System.out.println("Elige una de las opciones");
        //Elige con el teclado una de las opciones, del uno al tres, dependiendo de lo que quieras hacer. Cada paso te pedirá datos que tendrás que introducir de nuevo con el teclado. Pulsa el número 4 para salir y cerrar el programa
        System.out.println("Opción 1: cambio de horas a segundos\n Opción 2: cambio de kilómetros a metros\n Opción 3: Salir");
        System.out.println("Unidades Imperiales");
        System.out.println("Opción 4: cambio de pies a centímetros\n Opción 5: cambio de pulgadas a milímetros\n Opción 6: cambio de millas a kilómetros");
        
        switch(opc){
            case 1: System.out.println("Introduce las horas");
            hora=tc.nextDouble();
                System.out.println(hora+" horas son "+(hora*3600)+" segundos");
                break;
            case 2: System.out.println("Introduce los kilómetros");
                    kilometro=tc.nextDouble();
                    System.out.println(+kilometro+" kilómetros son "+(kilometro*1000)+" metros");
                    break;
            case 3:System.out.println("Introduce los kilómetros por hora");
            velocidad=tc.nextDouble();
                System.out.println(velocidad+" km/h son "+(velocidad/3.6)+" m/s");
            case 4: System.out.println("Introduce los pies");
                    pie=tc.nextDouble();
                    System.out.println(pie+" pies son "+(pie*30.48)+" centímetros");
                    break;
            case 5: System.out.println("Introduce las pulgadas");
                    pulgada=tc.nextDouble();
                    System.out.println(pulgada+" pulgadas son "+(pulgada*25.4)+" milímetros");
                    break;
            case 6: System.out.println("Introduce las millas");
                    milla=tc.nextDouble();
                    System.out.println(milla+" millas son "+(milla*1.6093)+" kilómetros");
            case 7: System.out.println("Saliendo");
            break;
            default: System.out.println("Opción incorrecta");
                
        }
        }while(opc!=4);
    }
}

